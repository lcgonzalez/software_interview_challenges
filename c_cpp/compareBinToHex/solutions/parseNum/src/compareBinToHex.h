/** Add solution title here
 *
 * Add solution description here
 *
 **/

#ifndef COMPAREBINTOHEX_H
#define COMPAREBINTOHEX_H

#include <string>       // std::string
#include <math.h>

int getDigit(char ch){ 
  if(ch>='0' && ch<='9') return ch-'0';
  else if(ch>='A' && ch<='F') return 10+ch-'A';
  else if(ch>='a' && ch<='f') return 10+ch-'a';
  else return -1;
}

int parseNum(std::string number, int base) {
  if(base<2 || (base>10 && base!=16)) 
    return -1;
  int value=0;
  for(int i=number.length()-1; i>=0; i--){
    int digit = getDigit(number.at(i));
    if(digit <0 || digit >=16)
      return -1;
    int exp = number.length()-1-i;
    value += digit*pow(base,exp);
  }
  return value;
}

bool compareBinToHex(std::string bin, std::string hex){
  int n1 = parseNum(bin, 2);
  int n2 = parseNum(hex,16);

  if(n1<0 || n2 <0) {
    return false;
  } else {
    return n1==n2;
  }
}

#endif // COMPAREBINTOHEX_H
