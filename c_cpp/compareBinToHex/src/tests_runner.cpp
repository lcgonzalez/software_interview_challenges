/** Anagram Strings
 *
 * Write a function to check if the value of a binary number 
 * (passed as a string) equals the hexadecimal representation
 * of a string.
 *
 * Good luck!!
 **/

// Test cases
#include "compareBinToHex.h"
#include <gmock/gmock.h>    // Testing framework
#include <fstream>          // std::ifstream
#include <string>           // std::string


class compareBinToHexTest : public ::testing::Test {
  public:
    void SetUp() {
    }
    void TearDown() {
    }
};

TEST_F(compareBinToHexTest, simpleTest)
{
  ASSERT_THAT(compareBinToHex("1111","F"), ::testing::Eq(true));
}

// Function that displays the first commentaries block of this file (problem statement)
void help(){
  std::string line;
  std::ifstream problemStatementFile("../src/tests_runner.cpp");
  bool inBlock = false;
  if(problemStatementFile.is_open()) {
    while(getline(problemStatementFile, line)) {
      if(line.find("/**") != std::string::npos) { // Find problem statement title
        inBlock = true;
        line.replace(line.find("/**"), 3, "");    // Ignore "/**"
        std::cout << line << std::endl;
      } else if(line.find("**/") != std::string::npos) { // End of problem statement
        inBlock = false;
        break;
      }
      if(inBlock && line.find(" *")!= std::string::npos) { // Problem statement body
        line.replace(line.find(" *"), 2, "");    // Ignore " *"
        std::cout << line << std::endl;
      }
    }
  }     
  problemStatementFile.close();
}


int main(int argc, char **argv) {
  help();
  ::testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
