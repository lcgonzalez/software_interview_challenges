/** Use stack to push and pop every matching open-close parenthesis
 *
 * This problem is another test for the data structures handling.
 * If the data structures are well known, this problem would be
 * solved very straightforwardly.
 *
 * The key to this challenge is the stack data structure.
 * First the input string would be scanned for ocurrences of
 * opening and closing parenthesis, on every opening parenthesis
 * ('(', '[', '{'), the parenthesis will be pushed into the stack.
 * When in a closing parenthesis (')', ']', '}'), the size of the
 * stack would be checked, if zero then it means that the parenthesis
 * are not balanced. The content of the last pushed parenthesis would
 * be also checked, if the last parenthesis isn't the corresponding
 * opening pair, then the parenthesis are again unbalanced.
 * At the end of the string scan, if the stack is empty, then
 * the parenthesis are balanced, and if it isn't empty, then
 * the parenthesis are not balanced. 
 *
 **/

#ifndef BALANCEDPARENTHESIS_H
#define BALANCEDPARENTHESIS_H

#include <string>   // std::string
#include <stack>    // std::stack

bool balancedParenthesis(std::string input) {
  std::stack<char> stack1;
  for(std::string::iterator strIt=input.begin();
      strIt!=input.end(); strIt++) {
    switch (*strIt) {
      case '(' :
        stack1.push('(');
        break;
      case ')' :
        if(stack1.size() == 0 || stack1.top() != '(') {   // Check if stack is empty or there
          return false;                                   // is no matching closing parenthesis
        }
        stack1.pop();
        break;
      case '[' :
        stack1.push('[');
        break;
      case ']' :
        if(stack1.size() == 0 || stack1.top() != '[')
          return false;
        stack1.pop();
        break;
      case '{' :
        stack1.push('{');
        break;
      case '}' :
        if(stack1.size() == 0 || stack1.top() != '{')
          return false;
        stack1.pop();
        break;
      default :
        break;
    }
  }
  if(stack1.size() != 0) {            // Check if stack is empty at the end of the string analysis
    return false;
  }
  return true;
}

#endif // BALANCEDPARENTHESIS_G
