/** Check balanced parentheses and brackets
 *
 * Given a string of opening and closing parentheses, check whether
 * it’s balanced. 
 * We have 3 types of parentheses: round brackets: (), square
 * brackets: [], and curly brackets: {}. 
 * Just to remind, balanced parentheses require every opening
 * parenthesis to be closed in the reverse order opened. 
 * For example ‘([])’ is balanced but ‘([)]‘ is not.
 *
 * Function signature:
 * bool balancedParenthesis(std::string input);
 *
 * Examples/Test cases:
 * balancedParenthesis("3*(a+4)+5") -> true
 * balancedParenthesis(")()(") -> false
 * balancedParenthesis("3*(a+(4*z)))") -> false
 * balancedParenthesis("3*(a+(4*z)") -> false
 * balancedParenthesis("3*[a+(4*z)+5]") -> true
 * balancedParenthesis("{[()()][]}") -> true
 * balancedParenthesis("{[()()][}]") -> false
 * balancedParenthesis("{[()()][[]}") -> false
 *
 * Good luck!
 **/
#include "balancedParenthesis.h"
#include <gmock/gmock.h>          // Testing framework
#include <fstream>                // std::ifstream
#include <string>                 // std::string

// Test cases
class balancedParenthesisTest : public ::testing::Test {
  public:
    void SetUp() {
    }
    void TearDown() {
    }
};

TEST_F(balancedParenthesisTest, noParenthesisTest)
{
  ASSERT_THAT(balancedParenthesis("3*a+4"), ::testing::Eq(true));
}

TEST_F(balancedParenthesisTest, simplePairTest)
{
  ASSERT_THAT(balancedParenthesis("3*(a+4)+5"), ::testing::Eq(true));
}

TEST_F(balancedParenthesisTest, unorderedPairTest)
{
  ASSERT_THAT(balancedParenthesis(")()("), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, unbalancedClosingTest)
{
  ASSERT_THAT(balancedParenthesis("3*(a+(4*z)))"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, unbalancedOpeningTest)
{
  ASSERT_THAT(balancedParenthesis("3*(a+(4*z)"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, balancedParenthesisAndBrackets1Test)
{
  ASSERT_THAT(balancedParenthesis("3*[a+(4*z)+5]"), ::testing::Eq(true));
}

TEST_F(balancedParenthesisTest, balancedParenthesisAndBrackets2Test)
{
  ASSERT_THAT(balancedParenthesis("{[()()][]}"), ::testing::Eq(true));
}

TEST_F(balancedParenthesisTest, unbalancedParenthesisAndBrackets1Test)
{
  ASSERT_THAT(balancedParenthesis("{[(])()[]}"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, unbalancedParenthesisAndBrackets2Test)
{
  ASSERT_THAT(balancedParenthesis("{[()()][}]"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, unorderedSquareBracketsTest)
{
  ASSERT_THAT(balancedParenthesis("{[()()]][}"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, extraOpeningSquareBracketsTest)
{
  ASSERT_THAT(balancedParenthesis("{[()()][[]}"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, extraClosingSquareBracketsTest)
{
  ASSERT_THAT(balancedParenthesis("{[()()][]]}"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, unorderedBracketsTest)
{
  ASSERT_THAT(balancedParenthesis("}{[()()][]"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, extraOpeningBracketsTest)
{
  ASSERT_THAT(balancedParenthesis("{{[()()][]}"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, extraClosingBracketsTest)
{
  ASSERT_THAT(balancedParenthesis("{[()()][]}}"), ::testing::Eq(false));
}

// Function that displays the first commentaries block of this file (problem statement)
void help(){
  std::string line;
  std::ifstream problemStatementFile("../src/tests_runner.cpp");
  bool inBlock = false;
  if(problemStatementFile.is_open()) {
    while(getline(problemStatementFile, line)) {
      if(line.find("/**") != std::string::npos) { // Find problem statement title
        inBlock = true;
        line.replace(line.find("/**"), 3, "");    // Ignore "/**"
        std::cout << line << std::endl;
      } else if(line.find("**/") != std::string::npos) { // End of problem statement
        inBlock = false;
        break;
      }
      if(inBlock && line.find(" *")!= std::string::npos) { // Problem statement body
        line.replace(line.find(" *"), 2, "");    // Ignore " *"
        std::cout << line << std::endl;
      }
    }
  }     
  problemStatementFile.close();
}


int main(int argc, char **argv) {
  help();
  ::testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
