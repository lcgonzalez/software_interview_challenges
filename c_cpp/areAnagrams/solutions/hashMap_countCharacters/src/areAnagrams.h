/** Hashtable (map) approach
 *
 * First the letters are extracted from the original strings
 * (thus ignoring punctuation) and the capital letters are
 * turned to lowercase.
 * Then the magic is on using a hashtable to count the number 
 * of characters and comparing the two hastables, if equal the 
 * two strings are anagrams, if different they are not.
 */
#ifndef AREANAGRAMS_H
#define AREANAGRAMS_H

#include <string>       // std::string
#include <stdexcept>    // std::invalid_argument
#include <map>          // std::map

std::string getLetters(std::string input) {
  char tmpChar;
  std::string output;
  for(std::string::iterator strIt=input.begin();
      strIt != input.end(); strIt++) {
    tmpChar = *strIt;
    if((tmpChar >= 'A' && tmpChar <= 'Z') 
      || (tmpChar >= 'a' && tmpChar <= 'z')) {  // Just extract the letters
      output = output + tmpChar;
    }
  }
  return output;
}


void toLower(std::string &input){
  char tmpChar;
  for(std::string::iterator strIt=input.begin();
      strIt != input.end(); strIt++) {
    tmpChar = *strIt;
    tmpChar = (tmpChar >= 'A' && tmpChar <= 'Z') ?  // If it's a capital letter 
      tmpChar-('A'-'a') : tmpChar;                  // To Lowercase
    *strIt = tmpChar;
  }
}

bool areAnagrams(std::string str1, std::string str2) {
  if(str1.empty() && str2.empty()) {
    throw std::invalid_argument(std::string("The strings to check if") +
        " anagrams are empy!");
  } else if(str1.empty()) {
    throw std::invalid_argument("The first string is empty!");
  } else if(str2.empty()) {
    throw std::invalid_argument("The second string is empty!");
  } else {
    str1 = getLetters(str1);
    str2 = getLetters(str2);
    if(str1.size() != str2.size()) {
      return false;
    }
    toLower(str1);
    toLower(str2);
    std::map<char,int> str1CharCount, str2CharCount;
    for(std::string::iterator strIt=str1.begin();   // Count the characters of the first string
        strIt!=str1.end(); strIt++) {
      str1CharCount[*strIt]+=1;
    }
    for(std::string::iterator strIt=str2.begin();
        strIt != str2.end(); strIt++) {             // Count the characters of the second string
      str2CharCount[*strIt]+=1;
    }
    if(str1CharCount == str2CharCount) {
      return true;
    } else {
      return false;
    }
  }
}

#endif // AREANAGRAMS_H
