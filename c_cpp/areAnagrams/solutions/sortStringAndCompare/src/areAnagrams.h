/** Sort strings and compare them
 *
 * Using this approach, first the punctuation is removed from both
 * strings, and the capital letters are turned into lowercase, in
 * order to only end up with letters in both strings.
 * Then the strings are sorted from 'a' to 'z' (using std::sort),
 * once sorted, the strings are compared. If they are equal then
 * the strings are anagrams, if not equeal then they are not.
 **/    
#ifndef AREANAGRAMS_H
#define AREANAGRAMS_H

#include <string>       // std::string
#include <algorithm>    // std::sort
#include <stdexcept>    // std::invalid_argument

void toLower(std::string &str){
  char tmpChar;
  for(std::string::iterator strIt=str.begin();
      strIt < str.end(); strIt++) {
    tmpChar = *strIt;
    if(tmpChar>='A' && tmpChar<='Z') {    // If the char is a Capital letter
      tmpChar = tmpChar - ('A'-'a');      // Turn it to lowercase
      *strIt = tmpChar;
    }
  }
}

void removePunctuation(std::string &str) {
  char tmpChar;
  for(std::string::iterator strIt=str.begin();
      strIt < str.end(); strIt++) {
    tmpChar = *strIt;
    if(!(tmpChar>='a' && tmpChar<='z') &&
        !(tmpChar>='A' && tmpChar<='Z')) {  // If the char is not a letter
      str.erase(strIt);
    }
  }
}

bool areAnagrams(std::string str1, std::string str2) {
  if(str1.empty() && str2.empty()) {
    throw std::invalid_argument("Both strings are empty!");
  } else if(str1.empty()){
    throw std::invalid_argument("The first string is empty");
  } else if(str2.empty()) {
    throw std::invalid_argument("The second string is empty");
  }  
  removePunctuation(str1);
  removePunctuation(str2);
  toLower(str1);
  toLower(str2);
  std::sort(str1.begin(), str1.end());
  std::sort(str2.begin(), str2.end());
  if(str1 == str2) {
    return true;
  } else {
    return false;
  }
}

#endif // AREANAGRAMS_H
