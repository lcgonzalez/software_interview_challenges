/** Anagram Strings
 *
 * Given two strings, check if they're anagrams or not. Two strings are
 * anagrams if they are written using the same exact letters, ignoring
 * space, punctiation and capitalization. Each letter should have the 
 * same count in both strings. For example, "Roma" and "Amor" are valid
 * anagrams, as well as "Eleven plus two" and "Twelve plus one".
 * Optional: If one or both input strings are empty, an exception should
 * be thrown.
 *
 * Function signature:
 * bool areAnagrams(std::string str1, std::string str2);
 *
 * Examples/Test cases:
 * areAnagrams("Roma", "Amor") -> true
 * areAnagrams("Eleven plus two", "Twelve plus one") -> true
 * areAnagrams("Elevenplus two", "Twelve plus one!") -> true
 * areAnagrams("hello", "which") -> false
 * Optional:
 * areAnagrams("","") -> throw std::invalid_argument("")
 * areAnagrams("hello","") -> throw std::invalid_argument("")
 * areAnagrams("","hello") -> throw std::invalid_argument("")
 *
 * Good luck!!
 **/

// Test cases
#include "areAnagrams.h"
#include <gmock/gmock.h>    // Testing framework
#include <fstream>          // std::ifstream
#include <string>           // std::string


class areAnagramsTest : public ::testing::Test {
  public:
    void SetUp() {
    }
    void TearDown() {
    }
};

TEST_F(areAnagramsTest, firstStringIsShorter_test)
{
  ASSERT_THAT(areAnagrams("aa","aaa"), ::testing::Eq(false));
}

TEST_F(areAnagramsTest, secondStringIsShorter_test)
{
  ASSERT_THAT(areAnagrams("aaa","aa"), ::testing::Eq(false));
}

TEST_F(areAnagramsTest, noAnagramWithoutSpacesAndPunctuation_test)
{
  ASSERT_THAT(areAnagrams("hello","which"), ::testing::Eq(false));
}

TEST_F(areAnagramsTest, noAnagramWithSpacesAndPuncuation_test)
{
  ASSERT_THAT(areAnagrams("hello world!","which names?"), ::testing::Eq(false));
}

TEST_F(areAnagramsTest, anagramWithoutSpacesAndPuncuation_test)
{
  ASSERT_THAT(areAnagrams("amor","roma"), ::testing::Eq(true));
}

TEST_F(areAnagramsTest, anagramWithSpacesAndPuncuation_test)
{
  ASSERT_THAT(areAnagrams("eleven plus two","twelve plus one"), ::testing::Eq(true));
}

TEST_F(areAnagramsTest, anagramWithSpacesPuncuationAndCapital_test)
{
  ASSERT_THAT(areAnagrams("Eleven plus two","Twelve plus one"), ::testing::Eq(true));
}

TEST_F(areAnagramsTest, anagramWithDifferentSpacesPuncuationAndCapital_test)
{
  ASSERT_THAT(areAnagrams("Elevenplus two","Twelve plus one!"), ::testing::Eq(true));
}

TEST_F(areAnagramsTest, emptyStrings_throwException)
{
  ASSERT_ANY_THROW(areAnagrams("",""));
}

TEST_F(areAnagramsTest, emptyFirstString_throwException)
{
  ASSERT_ANY_THROW(areAnagrams("","hello"));
}

TEST_F(areAnagramsTest, emptySecondString_throwException)
{
  ASSERT_ANY_THROW(areAnagrams("hello",""));
}

// Function that displays the first commentaries block of this file (problem statement)
void help(){
  std::string line;
  std::ifstream problemStatementFile("../src/tests_runner.cpp");
  bool inBlock = false;
  if(problemStatementFile.is_open()) {
    while(getline(problemStatementFile, line)) {
      if(line.find("/**") != std::string::npos) { // Find problem statement title
        inBlock = true;
        line.replace(line.find("/**"), 3, "");    // Ignore "/**"
        std::cout << line << std::endl;
      } else if(line.find("**/") != std::string::npos) { // End of problem statement
        inBlock = false;
        break;
      }
      if(inBlock && line.find(" *")!= std::string::npos) { // Problem statement body
        line.replace(line.find(" *"), 2, "");    // Ignore " *"
        std::cout << line << std::endl;
      }
    }
  }     
  problemStatementFile.close();
}


int main(int argc, char **argv) {
  help();
  ::testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
