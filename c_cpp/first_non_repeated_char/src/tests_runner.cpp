/** First non repeated character in string.
 *
 * Find the first non-repeated (unique) character in a given string.
 *
 * Function signature:
 * char firstNonRepeatedChar(std::string input);
 *
 * Examples/Test cases:
 * firstNonRepeatedChar("aabccbdeefgf") -> 'd'
 * firstNonRepeatedChar("aabccdeefgf") -> 'b'
 *
 * Good luck!!
 **/

// Test cases
#include "firstNonRepeatedChar.h"
#include <gmock/gmock.h>            // Testing framework
#include <fstream>                  // std::ifstream
#include <string>                   // std::string

class firstNonRepeatedCharTest : public ::testing::Test {
  public:
    void SetUp(){
    } 
    void TearDown() {
    }
};

TEST_F(firstNonRepeatedCharTest, emptyString_throwException)
{
  ASSERT_ANY_THROW(firstNonRepeatedChar(""));
}

TEST_F(firstNonRepeatedCharTest, test1)
{
  ASSERT_THAT(firstNonRepeatedChar("aabccbdeefgf"), ::testing::Eq('d'));
}

TEST_F(firstNonRepeatedCharTest, test2)
{
  ASSERT_THAT(firstNonRepeatedChar("aabccdeefgf"), ::testing::Eq('b'));
}

// Print the problem statement found in the header of this file
void help(){
  std::string line;
  std::ifstream problemStatementFile("../src/tests_runner.cpp");
  bool inBlock = false;
  if(problemStatementFile.is_open()) {
    while(getline(problemStatementFile, line)) {
      if(line.find("/**") != std::string::npos) { // Find problem statement title
        inBlock = true;
        line.replace(line.find("/**"), 3, "");    // Ignore "/**"
        std::cout << line << std::endl;
      } else if(line.find("**/") != std::string::npos) { // End of problem statement
        inBlock = false;
        break;
      }
      if(inBlock && line.find(" *")!= std::string::npos) { // Problem statement body
        line.replace(line.find(" *"), 2, "");    // Ignore " *"
        std::cout << line << std::endl;
      }
    }
  }     
  problemStatementFile.close();
}

int main(int argc, char **argv) {
  help();
  ::testing::InitGoogleMock(&argc,argv);
  return RUN_ALL_TESTS();
}
