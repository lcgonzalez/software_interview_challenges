/** Hashmap to count characters
 *
 * Working the solution out is very easily done 
 * with knowledge of the HashMap data structure.
 *
 * This approaches traverse the string, and
 * uses a hasmap (map) to count the ocurrences
 * of characters on the string. The characters
 * are the key to the map, while the value is
 * incremented by 1.
 * Finally, the string is traversed one more time
 * in which the characters are not counted,
 * but the characters are used as the key to the
 * map to check if the value stored (number of
 * a given character ocurrences on the string) is
 * equeal to 1, if so, that is the first non 
 * repeated character on the string. 
 *
 **/
#ifndef FIRSTNONREPEATEDCHAR_H
#define FIRSTNONREPEATEDCHAR_H

#include <string>       // std::string
#include <stdexcept>    // std::invalid_argument
#include <map>          // std::map

char firstNonRepeatedChar(std::string input) {
  if(input.empty()) {
    throw std::invalid_argument("Empty string");
  }
  std::map<char,int> charCounter;
  for(std::string::iterator strIt=input.begin();    // Count the number of individual chars
      strIt!=input.end(); strIt++) {                // on the string.
    charCounter[*strIt]+=1;
  }
  for(std::string::iterator strIt=input.begin();    // Check for the first character which
      strIt != input.end(); strIt++) {              // count is 1.
    if(charCounter[*strIt] == 1) {
      return *strIt;
    }
  }
}

#endif // FIRSTNONREPEATEDCHAR_H
