/** Implement queue
 *
 * Implement a queue with two stacks. Remember that a queue is an 
 * abstract data structure in which the first elemented inserted
 * (pushed) into it is the first element accessed (popped), or
 * in other words FIFA (First In, First Out).
 * The queue should have at least two methods, one to push new elements
 * and onde to pop the elements previously pushed.
 * The queue should also auto expand when it maxim size its reached.
 *
 * Interface:
 * template <class T>
 * class Queue {
 * public:
 *  Queue();
 *  void push(T newElement);
 *  T pop();
 * };
 *
 * Examples/Test cases:
 * Queue::push(5)
 * Queue::pop() -> 5
 *
 * Queue::push('h')
 * Queue::push('e')
 * Queue::push('l')
 * Queue::push('l')
 * Queue::push('o')
 * Queue::pop() -> 'h'
 * Queue::pop() -> 'e'
 * Queue::pop() -> 'l'
 * Queue::pop() -> 'l'
 * Queue::pop() -> 'o'
 *
 * Optional:
 * Queue::pop() -> throw std::invalid_argument("")
 *
 * Good luck!!
 **/

// Test cases
#include "queue.h"
#include <gmock/gmock.h>    // Testing framework
#include <fstream>          // std::ifstream
#include <string>           // std::string

class queueTest : public ::testing::Test {
  public:
    void SetUp() {
    }
    void TearDown() {
    }
};

TEST_F(queueTest, simplePushAndPop_test)
{
  Queue<int> queue1;
  int expected = 5;
  queue1.push(5);
  ASSERT_THAT(queue1.pop(), ::testing::Eq(expected));
}

TEST_F(queueTest, complexPushAndPop_test)
{
  Queue<char> queue1;
  char expectedArray[] = {'h','e','l','l','o',0};
  char tmpCharArray[6] = {'f','i','h','e','l',0};
  for(int i=0; i<5; i++) {
    queue1.push(tmpCharArray[i]);
  }
  queue1.pop();
  queue1.pop();
  queue1.push('l');
  queue1.push('o');
  char outputArray[6];
  outputArray[5] = 0;
  for(int i=0; i<5; i++) {
    outputArray[i] = queue1.pop();
  }
  ASSERT_STREQ(expectedArray, outputArray);
}

TEST_F(queueTest, autoExpandQueue_test)
{
  Queue<char> queue1(2);
  char expectedArray[] = {'h','e','l','l','o',0};
  for(int i=0; i<5; i++) {
    queue1.push(expectedArray[i]);
  }
  char outputArray[6];
  outputArray[5] = 0;
  for(int i=0; i<5; i++) {
    outputArray[i] = queue1.pop();
  }
  ASSERT_STREQ(expectedArray, outputArray);
}

TEST_F(queueTest, emptyQueuePop_throwException)
{
  Queue<int> queue1;
  ASSERT_ANY_THROW(queue1.pop());
}

// Function that displays the first commentaries block of this file (problem statement)
void help(){
  std::string line;
  std::ifstream problemStatementFile("../src/tests_runner.cpp");
  bool inBlock = false;
  if(problemStatementFile.is_open()) {
    while(getline(problemStatementFile, line)) {
      if(line.find("/**") != std::string::npos) { // Find problem statement title
        inBlock = true;
        line.replace(line.find("/**"), 3, "");    // Ignore "/**"
        std::cout << line << std::endl;
      } else if(line.find("**/") != std::string::npos) { // End of problem statement
        inBlock = false;
        break;
      }
      if(inBlock && line.find(" *")!= std::string::npos) { // Problem statement body
        line.replace(line.find(" *"), 2, "");    // Ignore " *"
        std::cout << line << std::endl;
      }
    }
  }     
  problemStatementFile.close();
}


int main(int argc, char **argv) {
  help();
  ::testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
