/** Two stacks, fast pop
 *
 * In this solution, one array is used to hold the actual queue internal
 * data structure, in which the top element is the first one inserted
 * on the queue, and the bottom elemtn of this duty stack is the last
 * element added to the queue.
 * In order to achieve this queue data structure on the stack, a second
 * temporary stack is used every time a new element is pushed into the
 * queue.
 * When a new element is inserted into the queue, the contents of the
 * duty stack are transfered to the temporary stack (from top to bottom),
 * then once the duty stack is empty, the new element is pushed into this
 * stack, thus the last element ends on the bottom of the stack. Finally,
 * the contents of the temprorary stack are transfered back to the duty
 * duty stack (from top to bottom), hence ending with the first element
 * inserted into the queue at the top of the duty stack, reasembling
 * a queue itself.
 * The fast pop feature comes from the fact that, when executing a pop
 * operation upon the queue, it only pops the last element pushed into
 * the duty stack; while when it pushes a new element, the queue has
 * to proceed with the algorithm previous described. Hence, the pop 
 * operation is fast relative to the push operation.
 *
 **/

#ifndef QUEUE_H
#define QUEUE_H

#include <stdexcept>    // std::

template <class T>
class Stack {
  T *array;
  int maxSize;
  int size_;
  public:
    Stack();
    Stack(int newMaxSize);
    void push(T newElement);
    T pop();
    int size() {return size_;}
};

template <class T>
Stack<T>::Stack() :
  maxSize(10),
  size_(0),
  array(new T[maxSize])
{
}

template <class T>
Stack<T>::Stack(int newMaxSize) :
  maxSize(newMaxSize),
  size_(0),
  array(new T[maxSize])
{
}

template <class T>
void Stack<T>::push(T newElement){
  if(size_ >= maxSize) {             // Array at its max capacity
    maxSize += 10;
    T *tmpArray = new T[maxSize];   // Resize array
    for(int i=0; i < size_; i++) {   // Copy old array into new one
      tmpArray[i] = array[i];
    }
    delete[] array;
    array = tmpArray;
  }
  array[size_++] = newElement;
}

template <class T>
T Stack<T>::pop() {
  if(size_ <= 0)
    throw std::underflow_error("The stack is already empty");
  return array[--size_];
}

template <class T>
class Queue {
  Stack<T> dutyStack;
  Stack<T> tmpStack;
  public:
    Queue();
    Queue(int newMaxSize);
    void push(T newElement);
    T pop();
    int size();
};

template <class T>
Queue<T>::Queue()
{
}

template <class T>
Queue<T>::Queue(int newMaxSize) :
  dutyStack(newMaxSize),
  tmpStack(newMaxSize)
{
}

template <class T>
void Queue<T>::push(T newElement){
  while(dutyStack.size() > 0) {
    tmpStack.push(dutyStack.pop());
  }  
  dutyStack.push(newElement);
  while(tmpStack.size() > 0) {
    dutyStack.push(tmpStack.pop());
  }
}

template <class T>
T Queue<T>::pop() {
  return dutyStack.pop();
}

template <class T>
int Queue<T>::size() {
  return dutyStack.size();
}

#endif // QUEUE_H
