/** Circular list
 *
 * The key concept used to implement this solution is the CircularList.
 * As it name implies, this list ends where it starts, it's like a cricle,
 * so it never ends, it keeps going and going over the same values.
 * Hence, the circular list has two index, one for the top and the
 * the other for the bottom of the list. Every time a new element is
 * inserted into the queue (push) the top index of the circular list would
 * be used to insert the element and then increased, whereas an element
 * is removed from the queue (pop), it would be extracted using the 
 * bottom index, and the this last index is decresed.
 * Whenever the size of the queue reaches its maximum, the circular
 * list is automatically expanded. 
 *
 **/

#ifndef QUEUE_H
#define QUEUE_H

#include <stdexcept>    // std::underflow_error

template <class T>
class CircularArray {
  T *array_;
  int size_;
  int maxSize_;
  int topIndex_;
  int bottomIndex_;
  public:
    CircularArray();
    CircularArray(int newMaxSize);
    void pushTop(T newElement);
    T popBottom();
    int size();
};

template <class T>
CircularArray<T>::CircularArray() :
  maxSize_(10),
  size_(0),
  topIndex_(0),
  bottomIndex_(0),
  array_(new T[maxSize_])
{
}

template <class T>
CircularArray<T>::CircularArray(int newMaxSize) :
  maxSize_(newMaxSize),
  size_(0),
  topIndex_(0),
  bottomIndex_(0),
  array_(new T[maxSize_])
{
}

template <class T>
void CircularArray<T>::pushTop(T newElement) {
  if(size_ >= maxSize_) {     // Array out of size
    maxSize_ += 10;
    T *tmpArray = new T[maxSize_];
    for(int i=0; i< maxSize_; i++) {
      tmpArray[i] = array_[i];      // Copy old array to the new allocated one
    }
    delete[] array_;
    array_ = tmpArray;
  }
  topIndex_ += (topIndex_ >= maxSize_) ? -maxSize_ : 0; // Keep cylcing the index over the queue max size
  array_[topIndex_++] = newElement;
  size_++;
}

template <class T>
T CircularArray<T>::popBottom() {
  if(size_ <= 0) 
    throw std::underflow_error("The circular list is empty!");
  bottomIndex_ = topIndex_ - (size_--);
  bottomIndex_ += (bottomIndex_ < 0 ) ? maxSize_ : 0; // Never let the index go beyond [0,maxSize_]
  return array_[bottomIndex_];
}
template <class T>
int CircularArray<T>::size() {
  return size_;
}

template <class T>
class Queue {
  CircularArray<T> circularArray_;
  public:
    Queue();
    Queue(int newMaxSize);
    void push(T newElement);
    T pop();
    int size();
};

template <class T>
Queue<T>::Queue()
{
}

template <class T>
Queue<T>::Queue(int newMaxSize) :
  circularArray_(newMaxSize)
{
}

template <class T>
void Queue<T>::push(T newElement){
  circularArray_.pushTop(newElement);
}

template <class T>
T Queue<T>::pop() {
  if(circularArray_.size() <= 0 )
    throw std::underflow_error("The queue is already empty!");
  return circularArray_.popBottom();
}

template <class T>
int Queue<T>::size() {
  return circularArray_.size();
}

#endif // QUEUE_H
