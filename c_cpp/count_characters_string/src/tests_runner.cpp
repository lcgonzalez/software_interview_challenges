/** Count characters on a string
 *
 * Given a string, output a new string that contains each character
 * in the original string exactly once, followed by the frequency of
 * occurences of that character in the original string, and in
 * alphabetical order.
 * The input string will consist of ASCII characters and no spaces.
 * Lowecase and Capital characters are to be interpreted the same. 
 * For example 'a' and 'A' would be counted as the same character.
 * When printing out the characters, it should always be in lowercase.
 *
 * Function signature:
 * std::string countCharacters(std::string input);
 *
 * Examples/Test cases:
 * countCharacters("hello") -> "e1h1l2o1"
 * countCharacters("oblivion") -> "b1i2l1n1o2v1"
 * countCharacters("WaLl") -> "a1l2w1"
 *
 * Good luck!
 **/

#include "countCharacters.h"
#include <gmock/gmock.h>      // Testing framework
#include <fstream>            // std::ifstream
#include <string>             // std::string

class countCharactersTest : public ::testing::Test {
  public:
    void SetUp() {
    }
    void TearDown() {
    }
};

TEST_F(countCharactersTest, lowerCase1Test)
{
  ASSERT_THAT(countCharacters("hello"), ::testing::StrEq("e1h1l2o1"));
}

TEST_F(countCharactersTest, lowerCase2Test)
{
  ASSERT_THAT(countCharacters("oblivion"), ::testing::StrEq("b1i2l1n1o2v1"));
}

TEST_F(countCharactersTest, capitalAndLowerCaseTest)
{
  ASSERT_THAT(countCharacters("WaLl"), ::testing::StrEq("a1l2w1"));
}

// Function that displays the first commentaries' block of this file (problem statement)
void help(){
  std::string line;
  std::ifstream problemStatementFile("../src/tests_runner.cpp");
  bool inBlock = false;
  if(problemStatementFile.is_open()) {
    while(getline(problemStatementFile, line)) {
      if(line.find("/**") != std::string::npos) { // Find problem statement title
        inBlock = true;
        line.replace(line.find("/**"), 3, "");    // Ignore "/**"
        std::cout << line << std::endl;
      } else if(line.find("**/") != std::string::npos) { // End of problem statement
        inBlock = false;
        break;
      }
      if(inBlock && line.find(" *")!= std::string::npos) { // Problem statement body
        line.replace(line.find(" *"), 2, "");    // Ignore " *"
        std::cout << line << std::endl;
      }
    }
  }     
  problemStatementFile.close();
}


int main(int argc, char** argv) {
  help();
  ::testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
