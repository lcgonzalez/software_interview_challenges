/** Hashmap as the counting data structure
 *
 * This is yet another test for the correct data structure.
 * With knowledge of the HashMap or map data strucutre, this
 * problem is very easily solved.
 *
 * Just as a matter of standarization, the problem asks for
 * capital letters counted as lowercase letters, and the output
 * to be displayed in lowercase, so the first thing to do is
 * to turn all the capital letter to lowercase.
 * Then, the key to solve this problem is the map<char,int>,
 * which is a hashmap data structure reassembling an array, but
 * which keys would be the string letters. On every occurence of
 * a leter (key) the corresponding value would be incremented in 1.
 * Another advantage of the map structure is that it intrinsically
 * arranges its key elements from lower to higher values, or in
 * this example, form 'a' to 'z', so there is no need to sort
 * the characters from 'a' to 'z'.
 * In the end, the map is accessed (already sorted) and the
 * key-value pair is formated (using a stringstream) as easy
 * as it can be in one line.
 *
 **/

#ifndef COUNTCHARACTERS_H
#define COUNTCHARACTERS_H

#include <string>     // std::string
#include <map>        // std::map
#include <sstream>    // std::stringstream

std::string countCharacters(std::string input) {
  std::map<char, int> charCounter;
  for(std::string::iterator strIt=input.begin();
      strIt != input.end(); strIt++){
    char tmpChar = *strIt;
    tmpChar = (tmpChar >= 'A' && tmpChar<='Z') 
      ? tmpChar-('A'-'a') : tmpChar;            // Lowercase if not already 
    charCounter[tmpChar]++;                     // Count lowercase ocurrences
  }

  std::stringstream tmpSS;
  for(std::map<char,int>::iterator mapIt=charCounter.begin();
      mapIt != charCounter.end(); mapIt++) {
    tmpSS << mapIt->first << mapIt->second;     // Format output
  }
  return tmpSS.str();
}

#endif // COUNTCHARACTERS_H
