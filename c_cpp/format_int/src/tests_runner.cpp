/** Format int to string, introducing commas.
 *
 * Code a function that receives an int and format it to a string,
 * returning the string of numbers that it received, but with a comma ','
 * inserted every three numbers, from right to left.
 * The returned string should be a "C-style" string, which means
 * that it should end with a \0 characher.
 *
 * Function signature:
 * char* formatInt(int);
 *
 * Example/Test cases:
 * formatInt(0) -> "0"
 * formatInt(123) -> "123"
 * formatInt(1234) -> "1,234"
 * formatInt(123450) -> "123,450"
 * formatInt(12345678) -> "12,345,678"
 *
 * Good luck!!!
 **/

// Test cases
#include "formatInt.h"
#include <gmock/gmock.h>    // Testing framework
#include <fstream>          // std::ifstream
#include <string>           // std::string

class formatIntTest : public ::testing::Test {
  public:
    void SetUp(){
    }

    void TearDown(){
    }
};

using namespace std;
TEST_F(formatIntTest, numberZero)
{
  char expectedString[2] = {'0', 0};
  ASSERT_STREQ(expectedString, formatInt(0));
}

TEST_F(formatIntTest, threeDigits)
{
  char expectedString[4] = {'1','2','3', 0};
  ASSERT_STREQ(expectedString, formatInt(123));
}

TEST_F(formatIntTest, fourDigits)
{
  char expectedString[] = {'1',',','2','3','4',0};
  ASSERT_STREQ(expectedString, formatInt(1234));
}

TEST_F(formatIntTest, zeroEnding)
{
  char expectedString[] = {'1','2','3',',','4','5','0',0};
  ASSERT_STREQ(expectedString, formatInt(123450));
}

TEST_F(formatIntTest, eightDigits)
{
  char expectedString[] = {'1','2',',','3','4','5',',','6','7','8',0};
  ASSERT_STREQ(expectedString, formatInt(12345678));
}

// Print the problem statement found un the header of this file
void help(){
  std::string line;
  std::ifstream problemStatementFile("../src/tests_runner.cpp");
  bool inBlock = false;
  if(problemStatementFile.is_open()) {
    while(getline(problemStatementFile, line)) {
      if(line.find("/**") != std::string::npos) { // Find problem statement title
        inBlock = true;
        line.replace(line.find("/**"), 3, "");    // Ignore "/**"
        std::cout << line << std::endl;
      } else if(line.find("**/") != std::string::npos) { // End of problem statement
        inBlock = false;
        break;
      }
      if(inBlock && line.find(" *")!= std::string::npos) { // Problem statement body
        line.replace(line.find(" *"), 2, "");    // Ignore " *"
        std::cout << line << std::endl;
      }
    }
  }     
  problemStatementFile.close();
}


int main(int argc, char** argv) {
  help();
  ::testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
