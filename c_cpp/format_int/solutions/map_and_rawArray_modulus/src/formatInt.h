/** Map and raw char array to format
 *
 * This problem can be easily solved using the correct
 * data structure, which could be achieved by a HashMap,
 * and the correct operator (modulus operator).
 *
 * To solve this problem the int original number is traversed two
 * times, the first one is to count the number of digits
 * on the number, which would be used to allocate a raw
 * array of chars that would be filled (formated) on the
 * second traverse of the int number.
 * The key to this problem is getting the last digit of
 * the number (the least significative digit). To do this,
 * the operator modulus is the keystone. 
 *
 * Getting the least siginificant digit can be achieved by applying
 * the modulus 10 (or the corresponding numerical base
 * number, e.g 16 for the hex numerical system) to the
 * number. By doing this operation, the last digit
 * would be calculated.
 * Once the last digit is calculated, the original number
 * is intiger divided by 10 (or the correspondig numerical
 * base number) to eliminate this last digit of it,
 * The last operation is repeated until the original number
 * is equal/less than zero.
 * If an index is used to count the number of iterations
 * unitl the original number reaches zero, then the number
 * of digitls it contains will be known. This same
 * index could be used to know the number of commas and
 * when to insert them (every three digits)
 * into the formated output string.
 *
 * To format the output string, the original number is
 * traversed, and the least significant digit of it is
 * calculated using the modulus operation just described.
 * This least significant digit would be used as the key
 * to a map that returns the corresponding char (in ASCII)
 * that represents that digit.
 * This char would be added to the formated output array (C-string)
 * from last to first position (to preserve the order
 * of the digits in the original number), and a comma would
 * be inserted every 3 digits with the help of the modulus
 * operator.
 * Finally, the last element of the output array is filled 
 * with the null character to mark the end of the string.
 *
 **/

#ifndef FORMATINT_H
#define FORMATINT_H

#include <map>
#include <iostream>

void initMap(std::map<int,char> &map1){
  map1[0] = '0';
  map1[1] = '1';
  map1[2] = '2';
  map1[3] = '3';
  map1[4] = '4';
  map1[5] = '5';
  map1[6] = '6';
  map1[7] = '7';
  map1[8] = '8';
  map1[9] = '9'; 
}

char* formatInt(int num) {
  std::map<int,char> intToChar;
  initMap(intToChar);
  int numLen = 0;
  int numCommas = 0;
  int numCopy = num;
  do{                           // First get the number of digits that the oroginal
    numCopy /= 10;              // number has, in ordar to know how much space to
    numLen++;                   // allocate for the output array.
    if(numLen%3==0 && numCopy!=0){
      numCommas++;              // Count the number of commas ',' to add 
    }
  }while(numCopy > 0); 

  int strLen = numLen + numCommas;    // Total length, used to allocate the output array
  char *output = new char[strLen + 1];
  for(int j=1, i=strLen-1; i>=0; i--){
    output[i] = intToChar[num%10];    // Turn least significant digit to char
    num /= 10;                        // Get rid of the least significant digit
    if(j++%3==0 && num!=0) {
      output[--i] = ',';
    }
  }

  output[strLen] = 0;        // terminator character
  return output;
}

#endif // FORMATINT_H
