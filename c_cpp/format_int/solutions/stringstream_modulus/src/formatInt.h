/** Stringstream and Moudulus
 *
 * This approach can be understand very easily if the 
 * operation of the modulus operator is understood.
 *
 * The key is to get the least significant digit of the
 * original number, format it to its equivalent ASCII
 * char, and add a comma every tree numbers.
 * The least significant number can be caluclated using
 * the modulus operator. By modulus dividing the
 * original number, by the numerical system base used
 * (10 in the decimal, 16 on the hex, etc.) this can
 * be achieved. And then the original number (int)
 * is divided by the same numberical system base to
 * get rid of the last digit. 
 * This operation is repeated until the original number 
 * reaches zero.
 *
 * To format the number, the least significant digit
 * is formated using a stringstream, which automatically
 * turns the digits into strings, and this resulting
 * string is appended to itself. Every three digits,
 * with help of the modulus operator, a comma is inserted
 * into the string. 
 * Finally, the stringstream returns a string, which
 * would be reverse traversed to arrange the digits on 
 * the correct order and build the corresponding C-style
 * string.
 *
 **/

#ifndef FORMATINT_H
#define FORMATINT_H

#include <sstream>      // std::stringstream
#include <string>       // std::string

char* formatInt(int num){
  std::stringstream ss;
  if(num == 0) {        // If zero, return '0'
    ss << 0;
  }
  int unit=10;          // Numerical base used (10 for decimal, 16 for hex, etc)
  int numDigits=1;      // Used to count the number of digits on the number
  while(num > 0) {
    ss << num%unit;
    num = num / unit;
    if(numDigits++%3 == 0 && num > 0){  // Every 3 digits, except if there are no
      ss << ",";                        // more digits.
    }
  }
  std::string tmpStr = ss.str();
  char* output = new char[ss.str().size()+1];
  int i=0;
  for(std::string::iterator strIt = tmpStr.end()-1; // The -1 is for the string ending char (null)
      strIt != tmpStr.begin()-1; strIt--) {
    output[i++] = *strIt;
  }
  output[i] = 0;
  return output;
}

#endif // FORMATINT_H
