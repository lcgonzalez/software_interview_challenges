/** Count opening and closing parenthesis
 *
 * Although this problem can also be solved using a stack,
 * there is an easier and more efficient way of
 * doing it.
 * In this approach, the input string is scanned for every
 * instance of '(' and ')'.
 * The key is to count the ocurrences of the parenthesis.
 * Every opening parenthesis '(' will add +1 to the count,
 * while every closing one ')' will add -1 to the count.
 * If, at any given moment, the count is negative, it will
 * mean that there are more closing parenthesis than opening
 * ones. Also, if at the end of the counting, the count is
 * positive, it will mean that there were more opening 
 * parenthesis.
 * If the count was never below zero, and it remained zero
 * at the end of the countig, then the string has balanced
 * parenthesis.  
 *
 **/
#ifndef BALANCEDPARENTHESIS_H
#define BALANCEDPARENTHESIS_H

#include <string>     // std::string

bool balancedParenthesis(std::string input) {
  int count=0;
  for(std::string::iterator strIt=input.begin();
      strIt!=input.end(); strIt++) {
    if(*strIt == '('){
      count++;  
    } else if(*strIt == ')') {
      count--;
    }
    if(count < 0){          // If below zero at any given moment
      return false;         // The parenthesis are not balanced
    }
  }
  if(count != 0 ){          // If the total count is positive
    return false;           // The parenthesis are not balanced
  } else {
    return true;
  }
}

#endif // BALANCEDPARENTHESIS_G
