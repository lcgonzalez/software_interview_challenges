/** Stack match every opening and closing parenthesis
 *
 * This problem can be easily solutioned with knowledge
 * of data structures, specially the stack.
 *
 * On this approach, the string is analyzed for
 * occurences of '(' and ')'. 
 * Every ocurrence of '(' is pushed
 * into a stack, and then, on every occurrrence of ')'
 * the size of the stack is verified, if zero then
 * there are more ')', thus the parenthesis are
 * unbalanced. If not zero then the top element of the
 * stack is poped (which would be the opening pair '(' ).
 * On the other hand, if at the end of the
 * string analysis, the stack is not empty, then there
 * were more '(', so the parenthesis are unbalanced.
 * If the stack were never empty when poping, nor
 * it contained any parenthesis at the end of the
 * string analysis, then the parenthesis are balanced.
 *
 **/
#ifndef BALANCEDPARENTHESIS_H
#define BALANCEDPARENTHESIS_H

#include <string>   // std::string
#include <stack>    // std::stack

bool balancedParenthesis(std::string input) {
  std::stack<char> stack1;
  for(std::string::iterator strIt=input.begin();
      strIt < input.end(); strIt++) {
    switch (*strIt) {
      case '(' :
        stack1.push(*strIt);
        break;
      case ')' :
        if(stack1.empty()) 
          return false;
        stack1.pop();
        break;
      default :
        break;
    }
  }
  if(!stack1.empty())
    return false;
  else
    return true;
}

#endif // BALANCEDPARENTHESIS_G
