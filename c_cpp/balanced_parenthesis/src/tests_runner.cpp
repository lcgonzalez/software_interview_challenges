/** Check balanced parentheses 
 *
 * Given a string of opening and closing parentheses, check whether
 * it’s balanced or not. 
 * Just to remind, balanced parentheses require every opening
 * parenthesis to be closed in the reverse order opened. 
 * For example ‘(())’ is balanced but ‘)()(‘ is not.
 * 
 * Function signature:
 * bool balancedParenthesis(std::string input);
 * 
 * Examples/Test cases:
 * balancedParenthesis("3*(a+4)+5") -> true
 * balancedParenthesis(")()(") -> false
 * balancedParenthesis("3*(a+(4*z)))") -> false
 * balancedParenthesis("3*(a+(4*z)") -> false
 * 
 * Good luck!
 **/
#include "balancedParenthesis.h"
#include <gmock/gmock.h>g         // Testing framework
#include <fstream>                // std::ifstream
#include <string>                 // std::string

class balancedParenthesisTest : public ::testing::Test {
  public:
    void SetUp() {
    }
    void TearDown() {
    }
};

TEST_F(balancedParenthesisTest, simplePairTest)
{
  ASSERT_THAT(balancedParenthesis("3*(a+4)+5"), ::testing::Eq(true));
}

TEST_F(balancedParenthesisTest, unorderedPairTest)
{
  ASSERT_THAT(balancedParenthesis(")()("), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, unbalancedClosingTest)
{
  ASSERT_THAT(balancedParenthesis("3*(a+(4*z)))"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, unbalancedOpeningTest)
{
  ASSERT_THAT(balancedParenthesis("3*(a+(4*z)"), ::testing::Eq(false));
}

TEST_F(balancedParenthesisTest, complexBalancedTest)
{
  ASSERT_THAT(balancedParenthesis("f*(3*(a+(4*z))-n)"), ::testing::Eq(true));
}

void help(){
  std::string line;
  std::ifstream problemStatementFile("../src/tests_runner.cpp");
  bool inBlock = false;
  if(problemStatementFile.is_open()) {
    while(getline(problemStatementFile, line)) {
      if(line.find("/**") != std::string::npos) { // Find problem statement title
        inBlock = true;
        line.replace(line.find("/**"), 3, "");    // Ignore "/**"
        std::cout << line << std::endl;
      } else if(line.find("**/") != std::string::npos) { // End of problem statement
        inBlock = false;
        break;
      }
      if(inBlock && line.find(" *")!= std::string::npos) { // Problem statement body
        line.replace(line.find(" *"), 2, "");    // Ignore " *"
        std::cout << line << std::endl;
      }
    }
  }     
  problemStatementFile.close();
}

int main(int argc, char **argv) {
  help();
  ::testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
