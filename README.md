# README #

### What is this repository for? ###
This is a repository designed to mock the technical aspect of a software interview. Aimed to help people prepare for a technical interview with big tech firms.
It gives people the chance to practice the most common challenges (or their variants) asked on software interviews, including the validation (testing) of the answer.

The work done so far is presented in C++, on a Linux environment, using CMake and Make for the building process, and gmock (Google mock) for testing the accomplishment of the challenge. 
Work currently in progress to expand it with more C++ challenges, as well as other languages.

### How do I get set up? ###

The repository is arranged as a series of folders, where every challenge is awaiting, ready to be solved and tested for validation.
In order to view, solve, code and test the challenges, first clone and cd to the folder containing the desired challenge,

```
#!bash

git clone git@bitbucket.org:lcgonzalez/software_interview_challenges.git
cd software_interview_challenges/areAnagrams/
```

Then create a building directory and cd to it,

```
#!bash

mkdir build && cd build
```

Build the challenge (the first time this step is executed it requires Internet connection, in order to download the gmock framework)

```
#!bash

cmake ../ && make
```

Finally run the challenge

```
#!bash

./tests_runner
```


Once the challenge runs, its description as well as its tests' results would be printed to standard output (terminal), so the challenge would be presented once built and executed.
Once the challenge and a possible solution have been analyzed, in order to code the solution and put it to the test, edit the .h file that is found on the src/ directory, under the challenge directory,

```
#!bash

gedit ../src/*.h
```


To prove that the solution really works and fulfill the challenge requirements, recompile and run the test_runner file again to get the tests results on the terminal,

```
#!bash

make && ./test_runner
```

Just as a piece of advise, the solutions to the challenges comes inside a folder named "solutions", inside the challenges folders, however is it advisable that the student work their solutions out and try to implement them using one's own means first. The solutions should be taken as an advise or guide to solve the challenges, not as the ultimate true solution.


## DISCLAIMER ##

**The solutions and challenges hereby presented are just a suggestion, and should never be taken as real challenges or unique optimal solutions. The challenges and solutions discussed on this project are a compilation of similar frequently discussed challenges found on Internet, thus they can vary from those presented on real interviews. **



### Contribution ###

The ultimate goal of this project is to have a corss-platform framework where people can practice for their software interviews as real as possible.

It would be greatly appreciated if people helped to expand this repository with more challenges, either for C++ or any other language. Or if desired, the porting to other platforms (Windows and Mac OS preferably).

If you wish to contribute, fork and create a pull request. All pull requests would be checked.
Just keep the same directory and path layout for the contributions.

**Any help or feedback will be gratefully received.** 


### Contact ###

I'm not the guy that carry out technical interviews at some big tech firm, nor I'm a member of a hiring board that casts Hire-No hire votes at some huge tech firm. I'm just a regular guy, which loves computer science, trying to help other people, that also love CS, to land a job at some big tech firm.

If you have any comments, you can send me an email to: **lc.gonzalez23@gmail.com**